{
  description = "go-kata";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-22.11";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }: flake-utils.lib.eachDefaultSystem (system:
    let
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages.${system};

    in
    {
      devShells.default = pkgs.mkShell {
        buildInputs = with pkgs; [
          # go development
          go
          go-outline
          gopls
          gopkgs
          go-tools
          delve

          # nix
          nixpkgs-fmt
          rnix-lsp
        ];

        shellHook = ''
        '';
      };
    });
}
